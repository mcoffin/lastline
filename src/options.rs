use clap::{
    Clap,
    crate_version,
    crate_authors,
    crate_description,
};

#[derive(Debug, Clap)]
#[clap(version = crate_version!(), author = crate_authors!(), about = crate_description!())]
pub struct Options {
    #[clap(long, short = 'n', default_value = "1", about = "Number of lines to show")]
    count: usize,
}

impl Options {
    #[inline(always)]
    pub fn count(&self) -> usize {
        self.count
    }
}
