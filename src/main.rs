extern crate clap;
extern crate log;
extern crate env_logger;

pub(crate) mod options;
mod logging;
pub(crate) use options::*;

use clap::Clap;
use log::*;
use std::{
    collections::LinkedList,
    io,
    process,
};

trait PushRotate<T: Sized> {
    fn push_rotate(&mut self, count: usize, item: T);
}

impl<T: Sized> PushRotate<T> for LinkedList<T> {
    fn push_rotate(&mut self, count: usize, item: T) {
        use std::iter;
        self.extend(iter::once(item));
        if self.len() > count {
            self.pop_front();
        }
        assert!(self.len() <= count);
    }
}

fn real_main(options: &Options) -> io::Result<()> {
    use io::BufRead;
    let mut lines_to_show: LinkedList<String> = LinkedList::new();
    let stdin = io::stdin();
    let stdin = stdin.lock();
    for line in stdin.lines() {
        let line = line?;
        lines_to_show.push_rotate(options.count(), line);
    }
    for line in lines_to_show.iter() {
        println!("{}", line);
    }
    Ok(())
}

fn main() {
    logging::init();
    let options = Options::parse();
    debug!("options: {:?}", &options);
    if let Err(e) = real_main(&options) {
        error!("{}", &e);
        process::exit(1);
    }
}
