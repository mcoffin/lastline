use std::{
    env,
    ffi::OsStr,
    sync::Once,
};

static INIT_LOGGING: Once = Once::new();
pub const DEFAULT_LOG_LEVEL: &'static str = "info";

fn env_or_default<K: AsRef<OsStr>, V: AsRef<OsStr>, F>(key: K, value_fn: F) where
    F: FnOnce() -> Option<V>,
{
    let key = key.as_ref();
    if env::var_os(key).is_none() {
        if let Some(v) = value_fn() {
            env::set_var(key, v);
        }
    }
}

pub fn init() {
    INIT_LOGGING.call_once(|| {
        env_or_default("RUST_LOG", || Some(format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL)));
        env_logger::init();
    });
}
