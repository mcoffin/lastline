# lastline

Command line utility for showing the last lines from stdin

# Example Usage

```bash
# This will get the last 5 lines from `journalctl -b` that contain "set_load"
journalctl -b | grep -F 'set_load' | lastline -n 5

# This will print "bar" and then "baz"
bash -c 'for s in foo bar baz; do echo "$s"; done;' | lastline --count 2

# This will print just "baz" (the default value for count is "1")
bash -c 'for s in foo bar baz; do echo "$s"; done;' | lastline
```
